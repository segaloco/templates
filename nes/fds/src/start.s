.segment	"VECTORS"
	.addr	nmi
	.addr	start
	.addr	irq
; -----------------------------------------------------------
.code

start:
	rti

nmi:
	rti

irq:
	rti
